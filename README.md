# LIS4368 - Advancced Web Applications Development

## Christian Pelaez-Espinosa

### Assignment 3# 

> ERD Diagrams - designed and develop Web applications, created a (design) database solutions that interact with the Web application—and, in fact, the data
repository is the *core* of all Web applications. 

*Two Parts*

1. Docs
2. Images

#### README.md file should include the following items:


[mwb file](docs/a3.mwb)

[sql file](docs/a3.sql)
 


*Screenshot of img/a3.png
![a3.png](https://bitbucket.org/repo/gqpxB7/images/2318749943-a3.png)